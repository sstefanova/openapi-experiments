# ResponseMessages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | **object** |  | [optional] 
**warning** | **object** |  | [optional] 
**error** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

