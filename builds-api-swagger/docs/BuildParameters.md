# BuildParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_url** | **object** | URL to the public git repository that contains the source code to build  | 
**ref** | **object** | Source code reference to build (ex. a git branch name) | [optional] 
**envvars** | **object** | Environment variables and values to be set at build time | [optional] 
**image_name** | **object** | The name of the image to be built | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

