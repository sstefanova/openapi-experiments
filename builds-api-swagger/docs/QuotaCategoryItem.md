# QuotaCategoryItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **object** |  | [optional] 
**limit** | **object** |  | [optional] 
**used** | **object** |  | [optional] 
**available** | **object** |  | [optional] 
**capacity** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

