# BuildCondition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**BuildStatus**](BuildStatus.md) |  | [optional] 
**message** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

