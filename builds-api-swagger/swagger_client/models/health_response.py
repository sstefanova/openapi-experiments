# coding: utf-8

"""
    Toolforge builds API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 0.0.3
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class HealthResponse(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'status': 'object',
        'messages': 'ResponseMessages'
    }

    attribute_map = {
        'status': 'status',
        'messages': 'messages'
    }

    def __init__(self, status=None, messages=None):  # noqa: E501
        """HealthResponse - a model defined in Swagger"""  # noqa: E501
        self._status = None
        self._messages = None
        self.discriminator = None
        if status is not None:
            self.status = status
        if messages is not None:
            self.messages = messages

    @property
    def status(self):
        """Gets the status of this HealthResponse.  # noqa: E501


        :return: The status of this HealthResponse.  # noqa: E501
        :rtype: object
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this HealthResponse.


        :param status: The status of this HealthResponse.  # noqa: E501
        :type: object
        """

        self._status = status

    @property
    def messages(self):
        """Gets the messages of this HealthResponse.  # noqa: E501


        :return: The messages of this HealthResponse.  # noqa: E501
        :rtype: ResponseMessages
        """
        return self._messages

    @messages.setter
    def messages(self, messages):
        """Sets the messages of this HealthResponse.


        :param messages: The messages of this HealthResponse.  # noqa: E501
        :type: ResponseMessages
        """

        self._messages = messages

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(HealthResponse, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, HealthResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
