# swagger-client
No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

This Python package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 0.0.3
- Package version: 1.0.0
- Build package: io.swagger.codegen.v3.generators.python.PythonClientCodegen

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on Github, you can install directly from Github

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import swagger_client 
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import swagger_client
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
id = NULL # object | Id of the build

try:
    api_response = api_instance.cancel(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->cancel: %s\n" % e)

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.clean()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->clean: %s\n" % e)

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
id = NULL # object | Id of the build

try:
    api_response = api_instance.delete(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete: %s\n" % e)

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
id = NULL # object | Id of the build

try:
    api_response = api_instance.get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get: %s\n" % e)

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.healthcheck()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->healthcheck: %s\n" % e)

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.latest()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->latest: %s\n" % e)

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->list: %s\n" % e)

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
id = NULL # object | Id of the build
follow = false # object | Follow the logs (optional) (default to false)

try:
    api_response = api_instance.logs(id, follow=follow)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->logs: %s\n" % e)

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.metrics()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->metrics: %s\n" % e)

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.openapi()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->openapi: %s\n" % e)

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.quota()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->quota: %s\n" % e)

# Configure API key authorization: key
configuration = swagger_client.Configuration()
configuration.api_key['ssl-client-subject-dn'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ssl-client-subject-dn'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
body = swagger_client.BuildParameters() # BuildParameters | 

try:
    api_response = api_instance.start(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->start: %s\n" % e)
```

## Documentation for API Endpoints

All URIs are relative to *http://127.0.0.1:8000*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**cancel**](docs/DefaultApi.md#cancel) | **PUT** /v1/build/{id} | 
*DefaultApi* | [**clean**](docs/DefaultApi.md#clean) | **POST** /v1/clean | 
*DefaultApi* | [**delete**](docs/DefaultApi.md#delete) | **DELETE** /v1/build/{id} | 
*DefaultApi* | [**get**](docs/DefaultApi.md#get) | **GET** /v1/build/{id} | 
*DefaultApi* | [**healthcheck**](docs/DefaultApi.md#healthcheck) | **GET** /v1/healthz | 
*DefaultApi* | [**latest**](docs/DefaultApi.md#latest) | **GET** /v1/build/latest | 
*DefaultApi* | [**list**](docs/DefaultApi.md#list) | **GET** /v1/build | 
*DefaultApi* | [**logs**](docs/DefaultApi.md#logs) | **GET** /v1/build/{id}/logs | 
*DefaultApi* | [**metrics**](docs/DefaultApi.md#metrics) | **GET** /v1/metrics | 
*DefaultApi* | [**openapi**](docs/DefaultApi.md#openapi) | **GET** /openapi.json | 
*DefaultApi* | [**quota**](docs/DefaultApi.md#quota) | **GET** /v1/quota | 
*DefaultApi* | [**start**](docs/DefaultApi.md#start) | **POST** /v1/build | 

## Documentation For Models

 - [Build](docs/Build.md)
 - [BuildCondition](docs/BuildCondition.md)
 - [BuildLog](docs/BuildLog.md)
 - [BuildParameters](docs/BuildParameters.md)
 - [BuildStatus](docs/BuildStatus.md)
 - [CancelResponse](docs/CancelResponse.md)
 - [CleanResponse](docs/CleanResponse.md)
 - [DeleteResponse](docs/DeleteResponse.md)
 - [GetResponse](docs/GetResponse.md)
 - [HealthResponse](docs/HealthResponse.md)
 - [LatestResponse](docs/LatestResponse.md)
 - [ListResponse](docs/ListResponse.md)
 - [NewBuild](docs/NewBuild.md)
 - [Principal](docs/Principal.md)
 - [Quota](docs/Quota.md)
 - [QuotaCategory](docs/QuotaCategory.md)
 - [QuotaCategoryItem](docs/QuotaCategoryItem.md)
 - [QuotaResponse](docs/QuotaResponse.md)
 - [ResponseMessages](docs/ResponseMessages.md)
 - [StartResponse](docs/StartResponse.md)

## Documentation For Authorization


## key

- **Type**: API key
- **API key parameter name**: ssl-client-subject-dn
- **Location**: HTTP header


## Author


