# BuildParameters


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_url** | **str** | URL to the public git repository that contains the source code to build  | 
**ref** | **str** | Source code reference to build (ex. a git branch name) | [optional] 
**envvars** | **Dict[str, str]** | Environment variables and values to be set at build time | [optional] 
**image_name** | **str** | The name of the image to be built | [optional] 

## Example

```python
from openapi_client.models.build_parameters import BuildParameters

# TODO update the JSON string below
json = "{}"
# create an instance of BuildParameters from a JSON string
build_parameters_instance = BuildParameters.from_json(json)
# print the JSON string representation of the object
print(BuildParameters.to_json())

# convert the object into a dict
build_parameters_dict = build_parameters_instance.to_dict()
# create an instance of BuildParameters from a dict
build_parameters_from_dict = BuildParameters.from_dict(build_parameters_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


