# GetResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messages** | [**ResponseMessages**](ResponseMessages.md) |  | [optional] 
**build** | [**Build**](Build.md) |  | [optional] 

## Example

```python
from openapi_client.models.get_response import GetResponse

# TODO update the JSON string below
json = "{}"
# create an instance of GetResponse from a JSON string
get_response_instance = GetResponse.from_json(json)
# print the JSON string representation of the object
print(GetResponse.to_json())

# convert the object into a dict
get_response_dict = get_response_instance.to_dict()
# create an instance of GetResponse from a dict
get_response_from_dict = GetResponse.from_dict(get_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


