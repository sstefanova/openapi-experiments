# QuotaCategory


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**items** | [**List[QuotaCategoryItem]**](QuotaCategoryItem.md) |  | [optional] 

## Example

```python
from openapi_client.models.quota_category import QuotaCategory

# TODO update the JSON string below
json = "{}"
# create an instance of QuotaCategory from a JSON string
quota_category_instance = QuotaCategory.from_json(json)
# print the JSON string representation of the object
print(QuotaCategory.to_json())

# convert the object into a dict
quota_category_dict = quota_category_instance.to_dict()
# create an instance of QuotaCategory from a dict
quota_category_from_dict = QuotaCategory.from_dict(quota_category_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


