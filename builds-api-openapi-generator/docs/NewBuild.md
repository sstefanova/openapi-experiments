# NewBuild


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**parameters** | [**BuildParameters**](BuildParameters.md) |  | [optional] 

## Example

```python
from openapi_client.models.new_build import NewBuild

# TODO update the JSON string below
json = "{}"
# create an instance of NewBuild from a JSON string
new_build_instance = NewBuild.from_json(json)
# print the JSON string representation of the object
print(NewBuild.to_json())

# convert the object into a dict
new_build_dict = new_build_instance.to_dict()
# create an instance of NewBuild from a dict
new_build_from_dict = NewBuild.from_dict(new_build_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


