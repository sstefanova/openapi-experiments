# CleanResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messages** | [**ResponseMessages**](ResponseMessages.md) |  | [optional] 

## Example

```python
from openapi_client.models.clean_response import CleanResponse

# TODO update the JSON string below
json = "{}"
# create an instance of CleanResponse from a JSON string
clean_response_instance = CleanResponse.from_json(json)
# print the JSON string representation of the object
print(CleanResponse.to_json())

# convert the object into a dict
clean_response_dict = clean_response_instance.to_dict()
# create an instance of CleanResponse from a dict
clean_response_from_dict = CleanResponse.from_dict(clean_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


