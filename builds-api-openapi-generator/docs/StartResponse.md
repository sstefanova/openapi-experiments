# StartResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messages** | [**ResponseMessages**](ResponseMessages.md) |  | [optional] 
**new_build** | [**NewBuild**](NewBuild.md) |  | [optional] 

## Example

```python
from openapi_client.models.start_response import StartResponse

# TODO update the JSON string below
json = "{}"
# create an instance of StartResponse from a JSON string
start_response_instance = StartResponse.from_json(json)
# print the JSON string representation of the object
print(StartResponse.to_json())

# convert the object into a dict
start_response_dict = start_response_instance.to_dict()
# create an instance of StartResponse from a dict
start_response_from_dict = StartResponse.from_dict(start_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


