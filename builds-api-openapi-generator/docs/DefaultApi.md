# openapi_client.DefaultApi

All URIs are relative to *http://127.0.0.1:8000*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancel**](DefaultApi.md#cancel) | **PUT** /v1/build/{id}/cancel | 
[**clean**](DefaultApi.md#clean) | **POST** /v1/clean | 
[**delete**](DefaultApi.md#delete) | **DELETE** /v1/build/{id} | 
[**get**](DefaultApi.md#get) | **GET** /v1/build/{id} | 
[**healthcheck**](DefaultApi.md#healthcheck) | **GET** /v1/healthz | 
[**latest**](DefaultApi.md#latest) | **GET** /v1/build/latest | 
[**list**](DefaultApi.md#list) | **GET** /v1/build | 
[**logs**](DefaultApi.md#logs) | **GET** /v1/build/{id}/logs | 
[**metrics**](DefaultApi.md#metrics) | **GET** /v1/metrics | 
[**openapi**](DefaultApi.md#openapi) | **GET** /openapi.json | 
[**quota**](DefaultApi.md#quota) | **GET** /v1/quota | 
[**start**](DefaultApi.md#start) | **POST** /v1/build | 


# **cancel**
> CancelResponse cancel(id)



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.cancel_response import CancelResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    id = 'id_example' # str | Id of the build

    try:
        api_response = api_instance.cancel(id)
        print("The response of DefaultApi->cancel:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->cancel: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Id of the build | 

### Return type

[**CancelResponse**](CancelResponse.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The build was canceled |  -  |
**400** | Bad parameters passed |  -  |
**401** | Unauthorized |  -  |
**404** | The latest build was not found |  -  |
**409** | operation conflicts with the current state of a build(s) |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **clean**
> CleanResponse clean()



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.clean_response import CleanResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        api_response = api_instance.clean()
        print("The response of DefaultApi->clean:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->clean: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**CleanResponse**](CleanResponse.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns a summary of what was cleaned up. |  -  |
**401** | Unauthorized |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete**
> DeleteResponse delete(id)



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.delete_response import DeleteResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    id = 'id_example' # str | Id of the build

    try:
        api_response = api_instance.delete(id)
        print("The response of DefaultApi->delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Id of the build | 

### Return type

[**DeleteResponse**](DeleteResponse.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The build was deleted |  -  |
**400** | Bad parameters passed |  -  |
**401** | Unauthorized |  -  |
**404** | The latest build was not found |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get**
> GetResponse get(id)



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.get_response import GetResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    id = 'id_example' # str | Id of the build

    try:
        api_response = api_instance.get(id)
        print("The response of DefaultApi->get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Id of the build | 

### Return type

[**GetResponse**](GetResponse.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The build was found |  -  |
**400** | Bad parameters passed |  -  |
**401** | Unauthorized |  -  |
**404** | The latest build was not found |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **healthcheck**
> HealthResponse healthcheck()



### Example


```python
import openapi_client
from openapi_client.models.health_response import HealthResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        api_response = api_instance.healthcheck()
        print("The response of DefaultApi->healthcheck:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->healthcheck: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**HealthResponse**](HealthResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the health status of the service |  -  |
**503** | Returned when the service is not ready |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **latest**
> LatestResponse latest()



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.latest_response import LatestResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        api_response = api_instance.latest()
        print("The response of DefaultApi->latest:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->latest: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**LatestResponse**](LatestResponse.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The latest build |  -  |
**400** | Bad parameters passed |  -  |
**401** | Unauthorized |  -  |
**404** | The latest build was not found |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list**
> ListResponse list()



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.list_response import ListResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        api_response = api_instance.list()
        print("The response of DefaultApi->list:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**ListResponse**](ListResponse.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the list of builds |  -  |
**401** | Unauthorized |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **logs**
> BuildLog logs(id, follow=follow)



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.build_log import BuildLog
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    id = 'id_example' # str | Id of the build
    follow = False # bool | Follow the logs (optional) (default to False)

    try:
        api_response = api_instance.logs(id, follow=follow)
        print("The response of DefaultApi->logs:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->logs: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| Id of the build | 
 **follow** | **bool**| Follow the logs | [optional] [default to False]

### Return type

[**BuildLog**](BuildLog.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | returns newline separated sequence of \&quot;#/components/schemas/BuildLog\&quot; or \&quot;#/component/schemas/InternalError\&quot; if there was an error after the streaming has started (as the http status code can&#39;t be changed)  |  -  |
**400** | Bad parameters passed |  -  |
**401** | Unauthorized |  -  |
**404** | The latest build was not found |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **metrics**
> str metrics()



### Example


```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        api_response = api_instance.metrics()
        print("The response of DefaultApi->metrics:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->metrics: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the metrics of the service |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **openapi**
> object openapi()



### Example


```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        api_response = api_instance.openapi()
        print("The response of DefaultApi->openapi:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->openapi: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | returns the openapi definition |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **quota**
> QuotaResponse quota()



### Example


```python
import openapi_client
from openapi_client.models.quota_response import QuotaResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        api_response = api_instance.quota()
        print("The response of DefaultApi->quota:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->quota: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**QuotaResponse**](QuotaResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Returns the quota information |  -  |
**401** | Unauthorized |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **start**
> StartResponse start(new_build)



### Example

* Api Key Authentication (key):

```python
import openapi_client
from openapi_client.models.build_parameters import BuildParameters
from openapi_client.models.start_response import StartResponse
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://127.0.0.1:8000
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://127.0.0.1:8000"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: key
configuration.api_key['key'] = os.environ["API_KEY"]

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['key'] = 'Bearer'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    new_build = openapi_client.BuildParameters() # BuildParameters | 

    try:
        api_response = api_instance.start(new_build)
        print("The response of DefaultApi->start:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->start: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **new_build** | [**BuildParameters**](BuildParameters.md)|  | 

### Return type

[**StartResponse**](StartResponse.md)

### Authorization

[key](../README.md#key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | returns the newly created build information |  -  |
**400** | Bad parameters passed |  -  |
**401** | Unauthorized |  -  |
**404** | The latest build was not found |  -  |
**500** | An internal error happened |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

