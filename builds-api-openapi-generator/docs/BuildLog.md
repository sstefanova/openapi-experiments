# BuildLog


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line** | **str** |  | [optional] 

## Example

```python
from openapi_client.models.build_log import BuildLog

# TODO update the JSON string below
json = "{}"
# create an instance of BuildLog from a JSON string
build_log_instance = BuildLog.from_json(json)
# print the JSON string representation of the object
print(BuildLog.to_json())

# convert the object into a dict
build_log_dict = build_log_instance.to_dict()
# create an instance of BuildLog from a dict
build_log_from_dict = BuildLog.from_dict(build_log_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


