# QuotaCategoryItem


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**limit** | **str** |  | [optional] 
**used** | **str** |  | [optional] 
**available** | **str** |  | [optional] 
**capacity** | **str** |  | [optional] 

## Example

```python
from openapi_client.models.quota_category_item import QuotaCategoryItem

# TODO update the JSON string below
json = "{}"
# create an instance of QuotaCategoryItem from a JSON string
quota_category_item_instance = QuotaCategoryItem.from_json(json)
# print the JSON string representation of the object
print(QuotaCategoryItem.to_json())

# convert the object into a dict
quota_category_item_dict = quota_category_item_instance.to_dict()
# create an instance of QuotaCategoryItem from a dict
quota_category_item_from_dict = QuotaCategoryItem.from_dict(quota_category_item_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


