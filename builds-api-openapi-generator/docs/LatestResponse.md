# LatestResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messages** | [**ResponseMessages**](ResponseMessages.md) |  | [optional] 
**build** | [**Build**](Build.md) |  | [optional] 

## Example

```python
from openapi_client.models.latest_response import LatestResponse

# TODO update the JSON string below
json = "{}"
# create an instance of LatestResponse from a JSON string
latest_response_instance = LatestResponse.from_json(json)
# print the JSON string representation of the object
print(LatestResponse.to_json())

# convert the object into a dict
latest_response_dict = latest_response_instance.to_dict()
# create an instance of LatestResponse from a dict
latest_response_from_dict = LatestResponse.from_dict(latest_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


