# ResponseMessages

Lists of messages by priority, note that there will be a warning here when the endpoint is being deprecated with information on how to proceed to update it. 

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | **List[str]** |  | [optional] 
**warning** | **List[str]** |  | [optional] 
**error** | **List[str]** |  | [optional] 

## Example

```python
from openapi_client.models.response_messages import ResponseMessages

# TODO update the JSON string below
json = "{}"
# create an instance of ResponseMessages from a JSON string
response_messages_instance = ResponseMessages.from_json(json)
# print the JSON string representation of the object
print(ResponseMessages.to_json())

# convert the object into a dict
response_messages_dict = response_messages_instance.to_dict()
# create an instance of ResponseMessages from a dict
response_messages_from_dict = ResponseMessages.from_dict(response_messages_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


