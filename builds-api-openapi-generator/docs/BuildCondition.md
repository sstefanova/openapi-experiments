# BuildCondition


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**BuildStatus**](BuildStatus.md) |  | [optional] 
**message** | **str** |  | [optional] 

## Example

```python
from openapi_client.models.build_condition import BuildCondition

# TODO update the JSON string below
json = "{}"
# create an instance of BuildCondition from a JSON string
build_condition_instance = BuildCondition.from_json(json)
# print the JSON string representation of the object
print(BuildCondition.to_json())

# convert the object into a dict
build_condition_dict = build_condition_instance.to_dict()
# create an instance of BuildCondition from a dict
build_condition_from_dict = BuildCondition.from_dict(build_condition_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


