# coding: utf-8

"""
    Toolforge builds API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: 0.0.2
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.models.build_status import BuildStatus

class TestBuildStatus(unittest.TestCase):
    """BuildStatus unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testBuildStatus(self):
        """Test BuildStatus"""
        # inst = BuildStatus()

if __name__ == '__main__':
    unittest.main()
