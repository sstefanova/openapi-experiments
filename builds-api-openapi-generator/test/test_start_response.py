# coding: utf-8

"""
    Toolforge builds API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: 0.0.2
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.models.start_response import StartResponse

class TestStartResponse(unittest.TestCase):
    """StartResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> StartResponse:
        """Test StartResponse
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `StartResponse`
        """
        model = StartResponse()
        if include_optional:
            return StartResponse(
                messages = openapi_client.models.response_messages.ResponseMessages(
                    info = [
                        ''
                        ], 
                    warning = [
                        ''
                        ], 
                    error = [
                        ''
                        ], ),
                new_build = openapi_client.models.new_build.NewBuild(
                    name = '', 
                    parameters = openapi_client.models.build_parameters.BuildParameters(
                        source_url = '', 
                        ref = '', 
                        envvars = {
                            'key' : ''
                            }, 
                        image_name = '', ), )
            )
        else:
            return StartResponse(
        )
        """

    def testStartResponse(self):
        """Test StartResponse"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
